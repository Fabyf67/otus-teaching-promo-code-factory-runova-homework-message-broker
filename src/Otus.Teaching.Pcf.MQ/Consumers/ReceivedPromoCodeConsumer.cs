﻿using MassTransit;
using Otus.Teaching.Pcf.MQ;
using Otus.Teaching.Pcf.MQ.Contracts;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class ReceivedPromoCodeConsumer : IConsumer<PromoCodeContract>
    {
        private readonly IReceivedPromoCodeService _promoCodeService;


        public ReceivedPromoCodeConsumer(IReceivedPromoCodeService promoCodeService) : base()
        {
            this._promoCodeService = promoCodeService;
        }

        public async Task Consume(ConsumeContext<PromoCodeContract> context)
        {
            await this._promoCodeService.ExecuteAsync(context.Message);
        }
    }
}
