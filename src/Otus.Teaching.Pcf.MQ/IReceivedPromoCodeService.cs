﻿using Otus.Teaching.Pcf.MQ.Contracts;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.MQ
{
    public interface IReceivedPromoCodeService
    {
        public Task ExecuteAsync(PromoCodeContract promoCode);
    }
}