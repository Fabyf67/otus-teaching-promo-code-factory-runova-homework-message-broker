﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.MQ;
using Otus.Teaching.Pcf.MQ.Contracts;
using System;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class AdministrationPromoCodeService : IReceivedPromoCodeService
    {
        private readonly IRepository<Employee> _employeeRepository;


        public AdministrationPromoCodeService(IRepository<Employee> employeeRepository)
        {
            this._employeeRepository = employeeRepository;
        }

        public async Task ExecuteAsync(PromoCodeContract promoCode)
        {
            if (!promoCode.PartnerManagerId.HasValue)
                throw new ArgumentOutOfRangeException($"PartnerManagerId has not value");

            var id = promoCode.PartnerManagerId.Value;
            var employee = await this._employeeRepository.GetByIdAsync(id);
            if (employee == null)
                throw new ArgumentOutOfRangeException($"Employee[{id}]: not found");

            employee.AppliedPromocodesCount++;
            await this._employeeRepository.UpdateAsync(employee);
        }
    }
}
